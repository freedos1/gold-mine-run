#ifndef _BONES_H
#define _BONES_H

void bones_init(Entity *e);
void bones_update(Entity *e);

#endif /* _BONES_H */
