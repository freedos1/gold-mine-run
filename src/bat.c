#include <stdint.h>

#include "vga.h"
#include "map.h"
#include "entities.h"

#include "player.h"

#include "bat.h"

#define DIR_UP 0
#define DIR_DOWN 1

/* limit of the animation cycle */
#define MAX_FRAME 4

static const Rect frames[2 * 4] =
{
    /* right */
    { 64, 32, 144, 112 },
    { 80, 32, 144, 112 },
    { 96, 32, 144, 112 },
    { 80, 32, 144, 112 },

    /* left */
    { 64, 32, 144, 112 },
    { 80, 32, 144, 112 },
    { 96, 32, 144, 112 },
    { 80, 32, 144, 112 },
};

void bat_init(Entity *e)
{
    e->frames = (const Rect *)frames;
    e->update = bat_update;
    /* will control vertical movement */
    e->flags = DIR_UP;
}

void bat_update(Entity *e)
{
    if (e->delay & 1)
    {
        /* horizontal */
        if (e->dir == DIR_RIGHT)
        {
            if (map_is_blocked(e->x + 16, e->y + 7))
                e->dir = DIR_LEFT;
            else
                e->x++;
        }
        else
        {
            /* dir is LEFT */
            if (map_is_blocked(e->x - 1, e->y + 7))
                e->dir = DIR_RIGHT;
            else
                e->x--;
        }

        /* vertical */
        if (e->flags == DIR_DOWN)
        {
            if (map_is_blocked(e->x + 7, e->y + 16))
                e->flags = DIR_UP;
            else
                e->y++;
        }
        else
        {
            /* dir is UP */
            if (map_is_blocked(e->x + 7, e->y - 1))
                e->flags = DIR_DOWN;
            else
                e->y--;
        }
    }

    if (player_collision(e))
    {
        player_hit();
        /* change direction */
        e->dir ^= 1;
        e->flags ^= 1;
    }

    if (e->delay++ == WALK_DELAY - 2)
    {
        e->delay = 0;
        e->frame++;
        if (e->frame == MAX_FRAME)
            e->frame = 0;
    }
}
