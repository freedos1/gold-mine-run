#ifndef _DATA_H
#define _DATA_H

/* embedded data */

/* graphics */
extern const uint8_t binary_palette_start[];
extern const uint8_t binary_sprites_start[];
extern const uint8_t binary_tiles_start[];
extern const uint8_t binary_font_start[];
extern const uint8_t binary_title_start[];

/* intro map */
extern const uint8_t binary_intro_start[];

/* maps */
extern const uint8_t binary_stage01_start[];
extern const uint8_t binary_stage02_start[];
extern const uint8_t binary_stage03_start[];
extern const uint8_t binary_stage04_start[];
extern const uint8_t binary_stage05_start[];
extern const uint8_t binary_stage06_start[];
extern const uint8_t binary_stage07_start[];
extern const uint8_t binary_stage08_start[];
extern const uint8_t binary_stage09_start[];
extern const uint8_t binary_stage10_start[];
extern const uint8_t binary_stage11_start[];
extern const uint8_t binary_stage12_start[];
extern const uint8_t binary_stage13_start[];
extern const uint8_t binary_stage14_start[];
extern const uint8_t binary_stage15_start[];
extern const uint8_t binary_stage16_start[];
extern const uint8_t binary_stage17_start[];
extern const uint8_t binary_stage18_start[];
extern const uint8_t binary_stage19_start[];
extern const uint8_t binary_stage20_start[];
extern const uint8_t binary_stage21_start[];
extern const uint8_t binary_stage22_start[];
extern const uint8_t binary_stage23_start[];
extern const uint8_t binary_stage24_start[];
extern const uint8_t binary_stage25_start[];
extern const uint8_t binary_stage26_start[];
extern const uint8_t binary_stage27_start[];
extern const uint8_t binary_stage28_start[];
extern const uint8_t binary_stage29_start[];
extern const uint8_t binary_stage30_start[];

/* sound */
extern const uint8_t binary_music_start[];
extern const uint8_t binary_music_size;

extern const uint8_t binary_hit_efx_start[];
extern const uint8_t binary_death_efx_start[];
extern const uint8_t binary_jump_efx_start[];
extern const uint8_t binary_time_efx_start[];
extern const uint8_t binary_gold_efx_start[];
extern const uint8_t binary_pickup_efx_start[];
extern const uint8_t binary_warp_efx_start[];
extern const uint8_t binary_oneup_efx_start[];

extern const uint8_t binary_hit_efx_size;
extern const uint8_t binary_death_efx_size;
extern const uint8_t binary_jump_efx_size;
extern const uint8_t binary_time_efx_size;
extern const uint8_t binary_gold_efx_size;
extern const uint8_t binary_pickup_efx_size;
extern const uint8_t binary_warp_efx_size;
extern const uint8_t binary_oneup_efx_size;

#endif /* _DATA_H */
