#ifndef _KEYB_H
#define _KEYB_H

extern volatile uint8_t keys[0xff];

void keyb_init();
void keyb_free();

#define KEY_ESC 1
#define KEY_TAB 15
#define KEY_ENTER 28
#define KEY_SPACE 57

#define KEY_A 30
#define KEY_S 31
#define KEY_Z 44
#define KEY_X 45
#define KEY_P 25

#define KEY_UP 72
#define KEY_LEFT 75
#define KEY_RIGHT 77
#define KEY_DOWN 80

#define KEY_F1 59
#define KEY_F2 60
#define KEY_F3 61
#define KEY_F4 62
#define KEY_F5 63
#define KEY_F6 64
#define KEY_F7 65
#define KEY_F8 66
#define KEY_F9 67
#define KEY_F10 68
#define KEY_F11 87
#define KEY_F12 88

#endif /* _KEYB_H */
