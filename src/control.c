#include <stdint.h>

#include "keyb.h"
#include "joy.h"

#include "control.h"

static uint8_t joy = 0;

void control_init()
{
    joy = joy_detect();
}

uint8_t control_read()
{
    uint8_t r = CTL_NONE;

    if (keys[KEY_UP])
        r |= CTL_UP;
    if (keys[KEY_DOWN])
        r |= CTL_DOWN;
    if (keys[KEY_LEFT])
        r |= CTL_LEFT;
    if (keys[KEY_RIGHT])
        r |= CTL_RIGHT;
    if (keys[KEY_Z])
        r |= CTL_FIRE1;
    if (keys[KEY_X])
        r |= CTL_FIRE2;

    if (joy)
        r |= joy_read();

    return r;
}
