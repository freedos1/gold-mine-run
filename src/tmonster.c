#include <stdint.h>
#include <stdlib.h>

#include "vga.h"
#include "map.h"
#include "entities.h"
#include "game.h"

#include "player.h"

#include "tmonster.h"

#define WAIT 120
#define MOVE_TIME 90

static const Rect frames[2 * 4] =
{
    /* right */
    { 96, 16, 144, 112 },
    { 80, 48, 144, 112 },

    /* not used */
    { 0, 0, 144, 112 },
    { 0, 0, 144, 112 },

    /* left */
    { 96, 16, 144, 112 },
    { 96, 48, 144, 112 },

    /* not used */
    { 0, 0, 144, 112 },
    { 0, 0, 144, 112 }
};

void tmonster_init(Entity *e)
{
    e->ox = e->y = 16;
    e->oy = e->x = 32 + (rand() % 256);
    e->dir = e->x > 160 ? DIR_LEFT : DIR_RIGHT;
    e->frames = (const Rect *)frames;
    e->update = tmonster_wait_update;
}

void tmonster_wait_update(Entity *e)
{
    e->delay++;
    e->frame = (e->delay & 2) ? 1 : 0;

    if (e->delay == WAIT)
    {
        e->delay = 0;
        e->frame = 1;
        e->update = tmonster_update;
    }
}

void tmonster_update(Entity *e)
{
    e->delay++;
    if (e->delay == WAIT * 2)
        e->delay = 0;

    if (e->delay < MOVE_TIME)
    {
        if (e->x > player_x() && e->x > 0)
        {
            e->dir = DIR_LEFT;
            e->x--;
        }
        if (e->x < player_x() && e->x < (MAP_W * MAP_TILE_W) - 16)
        {
            e->dir = DIR_RIGHT;
            e->x++;
        }
        if (e->y > player_y() && e->y > 0)
            e->y--;
        if (e->y < player_y() && e->y < (MAP_H * MAP_TILE_H) - 16)
            e->y++;
    }

    if (player_collision(e))
    {
        player_hit();
        reset_time();
    }
}
