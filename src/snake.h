#ifndef _SNAKE_H
#define _SNAKE_H

void snake_init(Entity *e);
void snake_update(Entity *e);

#endif /* _SNAKE_H */
