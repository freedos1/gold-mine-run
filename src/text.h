#ifndef _TEXT_H
#define _TEXT_H

void put_text(uint16_t x, uint16_t y, const char *text, uint8_t color);

#endif /* _TEXT_H */
