#ifndef _CONTROL_H
#define _CONTROL_H

#define CTL_NONE    0
#define CTL_UP      1
#define CTL_DOWN    2
#define CTL_RIGHT   4
#define CTL_LEFT    8
#define CTL_FIRE1   16
#define CTL_FIRE2   32

void control_init();
uint8_t control_read();

#endif /* _CONTROL_H */
