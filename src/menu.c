#include <stdint.h>
#include <stdio.h>

#include "keyb.h"
#include "control.h"
#include "vga.h"
#include "sound.h"
#include "text.h"
#include "data.h"
#include "game.h"

#define CAST_LEN 6

static void render_hiscore()
{
    char b[10];

    sprintf(b, "HI %06li", get_hiscore());
    put_text(124, 10, b, 5);

    put_text(96, 180, "_2023 USEBOX.NET", 1);
}

static void render_menu()
{
    Rect dst = { 80, 45, 160, 38 };
    blit(binary_title_start, &dst);

    put_text(88, 110, "PRESS JUMP TO PLAY", 1);

    put_text(64, 140, "CODE, GRAPHICS AND SOUND", 2);
    put_text(96, 150, "JUAN J. MARTINEZ", 3);
}

/* names of the cast */
static const char *names[CAST_LEN] = {
    "MINER",
    "SNAKEY",
    "BATTY",
    "OLD MINER",
    "MR BONES",
    "TIME MONSTER",
};

/* sprites for the cast */
static const Rect sprites[CAST_LEN] = {
    { 0, 0, 144, 112 },
    { 0, 32, 144, 112 },
    { 64, 32, 144, 112 },
    { 0, 80, 144, 112 },
    { 0, 96, 144, 112 },
    { 80, 48, 144, 112 },
};

static void render_cast()
{
    put_text(128, 32, "THE CAST", 1);

    Rect dst = { 104, 50, 16, 16 };

    /* show the cast line by line */
    for (uint8_t i = 0; i < CAST_LEN; i++)
    {
        blitrc(binary_sprites_start, &sprites[i], &dst);
        put_text(dst.x + 16 + 8, dst.y + 6, names[i], i + 2);

        wait_vsync();
        blit_copy_all();

        wait_frames(32);

        dst.y += 20;
    }
}

uint8_t run_menu()
{
    uint16_t count = 0;
    uint8_t cast = 0;
    uint8_t wait;
    uint8_t ctl;

    blit_target(TARGET_SCREEN);
    while (1)
    {
        ctl = control_read();

        if (keys[KEY_ESC])
            return 0;

        if (ctl & CTL_FIRE1)
        {
            if (cast)
            {
                cast = 0;
                count = 0;
                goto redraw_menu;
            }

            while (ctl & CTL_FIRE1)
            {
                ctl = control_read();
                wait_vsync();
            }

            sound_play_efx(EFX_ONEUP);

            for (uint8_t i = 0; i < 32; i++)
            {
                put_text(88, 110, "PRESS JUMP TO PLAY", i & 2 ? 5 : 0);
                wait_vsync();
            }
            wait_frames(96);

            return 1;
        }

        /* change of screen, requires drawing */
        if (count == 0)
        {
redraw_menu:
            blit_target(TARGET_BUFFER);
            blit_erase(0);

            render_hiscore();
            if (cast)
                render_cast();
            else
            {
                wait = 0;
                render_menu();
            }

            wait_vsync();
            blit_copy_all();
            blit_target(TARGET_SCREEN);
        }

        /* if is not cast, we do the "blink" effect */
        if (!cast)
        {
            wait++;
            if (wait == 64 || wait == 80)
            {
                if (wait == 80)
                    wait = 0;

                put_text(88, 110, "PRESS JUMP TO PLAY", wait ? 0 : 1);
                wait_vsync();
            }
        }

        /* frames before changing screen */
        if (count++ == 324)
        {
            cast ^= 1;
            count = 0;
        }

        wait_vsync();
    }
}
