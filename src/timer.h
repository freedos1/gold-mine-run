#ifndef _TIMER_H
#define _TIMER_H

#include <stdbool.h>

/* updates 18.2 times per second */
extern volatile uint32_t ticks;

extern bool is_ticking;

void timer_init();
void timer_free();

void timer_user_fn(void (*fn)(void));

/* countdown clock */
void timer_start(uint8_t secs, volatile uint8_t *updated);
uint8_t timer_value();
void timer_stop();
void timer_resume();

#endif /* _TIMER_H */
