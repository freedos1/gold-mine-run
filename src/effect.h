#ifndef _EFFECT_H
#define _EFFECT_H

void effect_out_new(uint16_t x, uint16_t y);
void effect_out_init(Entity *e);
void effect_out_update(Entity *e);

#endif /* _EFFECT_H */
