#ifndef _VGA_H
#define _VGA_H

/* palette index to be used as transparent color */
#define TRANSPARENT 16

typedef struct {
    uint16_t x;
    uint16_t y;
    uint16_t w;
    uint16_t h;
} Rect;

uint8_t open_framebuffer();
void close_framebuffer();

uint8_t set_mode(uint8_t mode);
void wait_vsync();
void wait_frames(uint16_t frames);

/* the palette is expected to be 8 bit per color, and will be converted to VGA's 6 bit per color */
void set_palette(const uint8_t *palette);

#define TARGET_SCREEN 0
#define TARGET_BUFFER 1

void blit_target(uint8_t t);

void blit(const uint8_t *sprite, const Rect *dst);
/* used for text: skip transparent, ignore 0, replace any other color by c */
void blit_c(const uint8_t *sprite, const Rect *dst, uint8_t c);
/* in src w is sprite width, h is sprite height */
void blitrc(const uint8_t *sprite, const Rect *src, const Rect *dst);
void blit_erase(uint8_t c);

/* copy from back buffer to screen */
void blit_copy_all();
void blit_copy(const Rect *dst);
/* only for 16 width, w is ignored */
void blit_copy16(const Rect *dst);

#endif /* _VGA_H */
