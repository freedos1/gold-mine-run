#ifndef _JOY_H
#define _JOY_H

/* it also calibrates */
uint8_t joy_detect();

/* returns the bits specified in control.h */
uint8_t joy_read();

#endif /* _JOY_H */
