#ifndef _PIKCUP_H
#define _PIKCUP_H

void pickup_time_init(Entity *e);
void pickup_bonus_init(Entity *e);
void pickup_pickaxe_init(Entity *e);
void pickup_goldkey_init(Entity *e);
void pickup_silverkey_init(Entity *e);

void pickup_wait_update(Entity *e);
void pickup_in_update(Entity *e);
void pickup_update(Entity *e);

#endif /* _PIKCUP_H */
