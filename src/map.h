#ifndef _MAP_H
#define _MAP_H

#define MAP_TILE_W 8
#define MAP_TILE_H 8

#define MAP_W 40
#define MAP_H 23

#define MAP_OFFS_Y 16

#define MAP_TILESET_COLS 20

#define MAP_FIRST_BLOCKED 40
#define MAP_FIRST_DEADLY 100

void map_init(const uint8_t map[]);
void map_render();

/* for gold */
void map_erase();

uint8_t map_is_blocked(uint16_t x, uint16_t y);
uint8_t map_is_deadly(uint16_t x, uint16_t y);

uint8_t map_update_gold(uint16_t x, uint16_t y);

void map_open_goldkey();
void map_open_silverkey();

uint8_t map_is_complete();

#endif /* _MAP_H */
