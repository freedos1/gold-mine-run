#include <stdio.h>
#include <stdint.h>
#include <go32.h>
#include <dpmi.h>
#include <pc.h>

volatile uint8_t keys[0xff] = { 0 };

static _go32_dpmi_seginfo old_handler, new_handler;

static void keyb_handler()
{
    uint8_t k = inportb(0x60);

    if (k & 128)
        keys[k & 127] = 0;
    else
        keys[k] = 1;

    outportb(0x20, 0x20);
}

void keyb_init()
{
    _go32_dpmi_get_protected_mode_interrupt_vector(9, &old_handler);
    new_handler.pm_offset = (unsigned long)keyb_handler;
    new_handler.pm_selector = _go32_my_cs();
    _go32_dpmi_allocate_iret_wrapper(&new_handler);
    _go32_dpmi_set_protected_mode_interrupt_vector(9, &new_handler);
}

void keyb_free()
{
    if (_go32_dpmi_set_protected_mode_interrupt_vector(9, &old_handler) == -1)
        fprintf(stderr, "Failed to free the keyb :(\n");
    else
        _go32_dpmi_free_iret_wrapper(&new_handler);
}
