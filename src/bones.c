#include <stdint.h>
#include <stdlib.h>

#include "vga.h"
#include "map.h"
#include "entities.h"

#include "player.h"
#include "old.h"

#include "bones.h"

static const Rect frames[2 * 4] =
{
    /* right */
    { 0, 96, 144, 112 },
    { 16, 96, 144, 112 },
    { 0, 96, 144, 112 },
    { 32, 96, 144, 112 },

    /* left */
    { 48, 96, 144, 112 },
    { 64, 96, 144, 112 },
    { 48, 96, 144, 112 },
    { 80, 96, 144, 112 },
};

void bones_init(Entity *e)
{
    e->frames = (const Rect *)frames;
    e->update = bones_update;
}

void bones_update(Entity *e)
{
    if (abs(player_y() - e->y) < 8 && e->flags < ENEMY_JUMP_DELAY)
    {
        if (player_x() > e->x && e->dir != DIR_RIGHT)
            e->dir = DIR_RIGHT;
        if (player_x() < e->x && e->dir != DIR_LEFT)
            e->dir = DIR_LEFT;
    }

    old_update(e);
}
