#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "vga.h"
#include "entities.h"

#include "effect.h"

#define MAX_FRAME 3

static const Rect frames[2 * 4] =
{
    { 96, 16, 144, 112 },
    { 112, 16, 144, 112 },
    { 128, 16, 144, 112 },

    /* not used */
    { 0, 0, 144, 112 },
    { 0, 0, 144, 112 },
    { 0, 0, 144, 112 },
    { 0, 0, 144, 112 },
    { 0, 0, 144, 112 }
};

void effect_out_new(uint16_t x, uint16_t y)
{
    Entity *e = entities_new();
#ifdef DEBUG
    if (!e)
    {
        set_mode(3);
        fprintf(stderr, "ERROR: run out of entities\n");
        exit(1);
    }
#endif

    e->x = x;
    e->y = y;

    effect_out_init(e);
}

void effect_out_init(Entity *e)
{
    e->frames = (const Rect *)frames;
    e->dir = DIR_RIGHT;
    e->frame = 1;
    e->delay = 0;
    e->update = effect_out_update;
}

void effect_out_update(Entity *e)
{
    if (e->delay++ == WALK_DELAY - 2)
    {
        e->delay = 0;
        e->frame++;
        if (e->frame == MAX_FRAME)
            e->used = USED_FREE;
    }
}
