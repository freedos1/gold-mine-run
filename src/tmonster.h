#ifndef _TMONSTER_H
#define _TMONSTER_H

void tmonster_init(Entity *e);
void tmonster_wait_update(Entity *e);
void tmonster_update(Entity *e);

#endif /* _TMONSTER_H */
