#ifndef _PLAYER_H
#define _PLAYER_H

void player_init(uint16_t start_x, uint8_t start_y, uint8_t start_dir);
void player_update();

void player_erase();
void player_draw();

uint8_t player_collision(Entity *e);
uint8_t player_collision_pickup(Entity *e);
void player_hit();

void player_stageclear();
uint16_t player_x();
uint16_t player_y();

#define GRAVITY_OFF 0
/* XXX: substract 1 to get the value from gravity_seq */
#define GRAVITY_DOWN 14
#define GRAVITY_UP 1

#define GRAVITY_SEQ_LEN 24

extern const uint8_t gravity_seq[GRAVITY_SEQ_LEN];

#endif /* _PLAYER_H */
