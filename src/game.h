#ifndef _GAME_H
#define _GAME_H

void run_game();

void add_score(uint8_t v);
uint32_t get_hiscore();
uint8_t dec_lives();
void reset_time();
void add_pickaxe();
uint8_t use_pickaxe();
uint8_t is_stageclear();

#endif /* _GAME_H */
