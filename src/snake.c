#include <stdint.h>

#include "vga.h"
#include "map.h"
#include "entities.h"

#include "player.h"

#include "snake.h"

static const Rect frames[2 * 4] =
{
    /* right */
    { 0, 32, 144, 112 },
    { 16, 32, 144, 112 },
    /* not used */
    { 0, 0, 144, 112 },
    { 0, 0, 144, 112 },

    /* left */
    { 32, 32, 144, 112 },
    { 48, 32, 144, 112 },
    /* not used */
    { 0, 0, 144, 112 },
    { 0, 0, 144, 112 }
};

void snake_init(Entity *e)
{
    e->frames = (const Rect *)frames;
    e->update = snake_update;
}

void snake_update(Entity *e)
{
    if (e->delay & 1)
    {
        if (e->dir == DIR_RIGHT)
        {
            if (map_is_blocked(e->x + 16, e->y + 15)
                    || !map_is_blocked(e->x + 16, e->y + 16))
                e->dir = DIR_LEFT;
            else
                e->x++;
        }
        else
        {
            /* dir is LEFT */
            if (map_is_blocked(e->x - 1, e->y + 15)
                    || !map_is_blocked(e->x - 1, e->y + 16))
                e->dir = DIR_RIGHT;
            else
                e->x--;
        }
    }

    if (player_collision(e))
    {
        player_hit();
        /* change direction */
        e->dir ^= 1;
    }

    if (e->delay++ == WALK_DELAY + 4)
    {
        e->delay = 0;
        e->frame ^= 1;
    }
}
