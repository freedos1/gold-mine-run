#ifndef _SOUND_H
#define _SOUND_H

enum {
    EFX_GOLD = 0,
    EFX_JUMP,
    EFX_WARP,
    EFX_PICKUP,
    EFX_TIME,
    EFX_ONEUP,
    EFX_HIT,
    EFX_DEATH,
};

#define PAT_SILENCE     0
#define PAT_INTRO       1
#define PAT_READY       2
#define PAT_STAGE       3
#define PAT_GAMEOVER    4
#define PAT_PLAY        5

uint8_t sound_init();
void sound_free();
void sound_update();

void sound_music_pattern(uint16_t pat);
void sound_play_efx(uint8_t efxno);

void sound_mute();
void sound_unmute();

#endif /* _SOUND_H */
