#include <stdint.h>

#include "vga.h"
#include "data.h"

void put_text(uint16_t x, uint16_t y, const char *text, uint8_t color)
{
    Rect dst = { x, y, 8, 8};

    while (*text)
    {
        blit_c(binary_font_start + ((*text++ - ' ') << 6), &dst, color);
        dst.x += 8;
    }
}
