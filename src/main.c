#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <crt0.h>

#include "timer.h"
#include "keyb.h"
#include "sound.h"
#include "control.h"
#include "vga.h"
#include "data.h"
#include "menu.h"
#include "game.h"

#define GAME_NAME "Gold Mine Run!"
#define GAME_VERSION "1.8 (Oct 4 2023)"
#define GAME_URL "https://www.usebox.net/jjm/gold-mine-run/"

/* disable paging because our int handlers are written in C */
int _crt0_startup_flags = _CRT0_FLAG_LOCK_MEMORY;

static void run_help(const char *argv0)
{
    printf("usage: %s [OPTION]\n\n"
           "Valid options:\n\n"
           "  -h, /?  this help screen\n"
           "  -v      print version and exit\n"
           "  -ns     run the game with no sound\n\n"
           "More info and updates:\n\n"
           "  " GAME_URL "\n\n",
           argv0);
}

static void free_all()
{
    timer_free();
    keyb_free();
    sound_free();
}

int main(int argc, char *argv[])
{
    uint8_t nosound = 0;

    if (argc > 1)
    {
        for (uint8_t i = 1; i < argc; i++)
            if (!strcmp(argv[i], "-h")
                    || !strcmp(argv[i], "/?"))
            {
                run_help(argv[0]);
                return 0;
            }
            else if (!strcmp(argv[i], "-v"))
            {
                printf(GAME_NAME " version " GAME_VERSION "\n");
                return 0;
            }
            else if (!strcmp(argv[i], "-ns"))
                nosound = 1;
            else
            {
                fprintf(stderr, "ERROR: invalid option, try -h\n");
                return 1;
            }
    }

    if (!nosound && !sound_init())
    {
        fprintf(stderr, "ERROR: failed to init sound\n");
        return 1;
    }

    timer_init();
    keyb_init();
    atexit(free_all);

    /* requires keyboard */
    control_init();

    /* to update mikmod */
    if (!nosound)
        timer_user_fn(sound_update);

    /* set VGA 320x200, 256 col */
    if (!set_mode(0x13))
    {
        fprintf(stderr, "ERROR: failed to init the VGA card\n");
        return 1;
    }

    set_palette(binary_palette_start);

    if (!open_framebuffer())
    {
        set_mode(3);
        fprintf(stderr, "ERROR: failed to open the framebuffer\n");
        return 1;
    }

    while (run_menu())
        run_game();

    blit_target(TARGET_SCREEN);
    wait_vsync();
    blit_erase(0);

    set_mode(3);
    close_framebuffer();

    return 0;
}
