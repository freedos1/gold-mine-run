#ifndef _ENTITIES_H
#define _ENTITIES_H

#define DIR_RIGHT 0
#define DIR_LEFT 1

#define WALK_DELAY 8

#define USED_FREE   0
#define USED_BG     1
#define USED_FG     2
#define USED_PLAYER 3

typedef struct entity_s
{
    uint8_t used;
    uint16_t x;
    uint16_t y;
    uint8_t erase;
    uint16_t ox;
    uint16_t oy;
    uint8_t dir;
    uint8_t frame;
    uint8_t delay;
    uint8_t gravity;
    uint8_t flags;
    uint16_t counter;
    /* expected to be 2 directions per 4 frames max; 8 Rect */
    const Rect *frames;
    void (*update)(struct entity_s *e);
} Entity;

void entities_init();

Entity *entities_new();

void entities_update();
/* MUST be run before draw */
void entities_sort();
/* also erases/draws the player */
void entities_draw();
void entities_warp_out_all();

#endif /* _ENTITIES_H */
