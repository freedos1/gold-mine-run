all:
	make -j -C src all

run: all
	dosbox -config dosbox.conf

clean:
	make -C src clean

cleanall:
	make -C src clean

.PHONY: all clean run
