# Gold Mine Run!

This is an arcade platformer for DOS.

## Requirements

Running the game:

- MS/DOS or compatible
- A DOS Protected Mode Interface, e.g. [CWSDPMI](http://sandmann.dotster.com/cwsdpmi/)
- 386DX 33MHz (or better) with VGA
- At least 4MB of RAM
- Sound Blaster (or no sound)

The game can be controlled by keyboard or joystick.

| Key | Joystick | Action |
| --- | --- | --- |
| Cursor left | Joystick left | Move left |
| Cursor right | Joystick right | Move Right |
| Z | Button 1 | Jump |
| P | - | Pause / resume game |
| ESC | - | Abandon / exit the game |

Check `-h` CLI flag for options.

Development:

- GNU Make
- DJGPP C (using GCC 12.1.0; other may work)
- [libmikmod](https://mikmod.sourceforge.net/)
- DOSBOX
- tools: Python 3 and PIL (or Pillow) library

For DJGPP I recommend cross-compilation using [build-djgpp](https://github.com/andrewwutw/build-djgpp/).

Set `LIBMIKMOD_BASE` env variable to your libmikmod sources with the DOS library compiled and ready to use.

You can build the game with `make` and run it in DOSBOX with `make run`.

## Author

This was made by [Juan J. Martinez](https://www.usebox.net/jjm/about/me/).

* The code is licensed MIT, see [LICENSE](LICENSE) file.
* The assets are licensed [CC-SA-NC](https://creativecommons.org/licenses/by-sa/4.0/).

Because the assets are licensed CC-SA-NC, you are not allowed to use them in a
commercial project. In case you wanted to sell copies of **Gold Mine Run!**, you
would have to replace all the graphics, music, sound effects and level data.

If you include a substantial part of this project in your own work, you should
include a copy of the LICENSE file in your source code and clarify which files
are covered by it.

Homepage: [Gold Mine Run!](https://www.usebox.net/jjm/gold-mine-run/)

