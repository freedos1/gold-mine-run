#!/usr/bin/env python3

from argparse import ArgumentParser
import subprocess
import os
from PIL import Image

__version__ = "1.0"

ld = os.environ.get("LD", "i586-pc-msdosdjgpp-ld")
strip = os.environ.get("STRIP", "i586-pc-msdosdjgpp-strip")


def main():
    parser = ArgumentParser(
        description="PNG to pixel data .o",
        epilog="Copyright (C) 2023 Juan J Martinez <jjm@usebox.net>",
    )

    parser.add_argument(
        "--version", action="version", version="%(prog)s " + __version__
    )
    parser.add_argument("image", help="image to convert")
    parser.add_argument("output", help="object name for the pixel data")

    args = parser.parse_args()

    try:
        image = Image.open(args.image)
    except IOError:
        parser.error("failed to open the image")

    if image.mode != "P":
        parser.error("not an indexed image (no palette)")

    data = image.getdata()
    if not data:
        parser.error("failed to extract the pixel data")

    tmp = args.output.removesuffix(".o")
    with open(tmp, "wb") as fd:
        fd.write(bytearray(data))
        fd.flush()

        # create an object file from the binary
        rc = subprocess.call(
            [
                ld,
                "-r",
                "-b",
                "binary",
                "-o",
                args.output,
                tmp,
            ]
        )
        os.unlink(tmp)
        if rc != 0:
            parser.error("Failed to run %s" % ld)

        # strip unwanted symbols
        rc = subprocess.call([strip, "-w", "-K", "*_%s_*" % tmp, args.output])
        if rc != 0:
            parser.error("Failed to run %s" % ld)


if __name__ == "__main__":
    main()
