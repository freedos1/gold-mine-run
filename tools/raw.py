#!/usr/bin/env python3

from argparse import ArgumentParser
import subprocess
import os

__version__ = "1.0"

ld = os.environ.get("LD", "i586-pc-msdosdjgpp-ld")
strip = os.environ.get("STRIP", "i586-pc-msdosdjgpp-strip")


def main():
    parser = ArgumentParser(
        description="RAW to .o",
        epilog="Copyright (C) 2023 Juan J Martinez <jjm@usebox.net>",
    )

    parser.add_argument(
        "--version", action="version", version="%(prog)s " + __version__
    )
    parser.add_argument("file", help="file to convert")
    parser.add_argument("output", help="object name for the map data")

    args = parser.parse_args()

    with open(args.file, "rb") as fd:
        data = fd.read()

    tmp = args.output.removesuffix(".o")
    with open(tmp, "wb") as fd:
        fd.write(bytearray(data))
        fd.flush()

        # create an object file from the binary
        rc = subprocess.call(
            [
                ld,
                "-r",
                "-b",
                "binary",
                "-o",
                args.output,
                tmp,
            ]
        )
        os.unlink(tmp)
        if rc != 0:
            parser.error("Failed to run %s" % ld)

        # strip unwanted symbols
        rc = subprocess.call([strip, "-w", "-K", "*_%s_*" % tmp, args.output])
        if rc != 0:
            parser.error("Failed to run %s" % ld)


if __name__ == "__main__":
    main()
